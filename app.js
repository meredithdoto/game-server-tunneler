const SteamUser = require('steam-user');
const SteamTotp = require('steam-totp');
const Ngrok = require('ngrok');
const CP = require('child_process');
const Stdin = process.openStdin();
const Fs = require('fs');
const Config = JSON.parse(Fs.readFileSync('./config.json'));

const client = new SteamUser();

const logOnOptions = {
    accountName: Config.user,
    password: Config.password,
    twoFactorCode: SteamTotp.generateAuthCode(Config.secret),

    rememberPassword: true
};

/**
 * SERVER TUNNEL ANNOUNCE
 */
let server = CP.spawn(Config.server.interpret, Config.server.args, {cwd: process.cwd() + '/server'});
server.stdout.setEncoding('utf8');

(async function ()
{
    let url = await Ngrok.connect({
        proto: 'tcp',
        addr: Config.server.port,
        region: Config.server.region,
        authtoken: Config.server.authtoken
    });
    console.log('[NGROK] URL: ' + url);
    if (Config.useSteamChat)
    {
        client.logOn(logOnOptions);

        client.on('loggedOn', function ()
        {
            console.log('[STEAM] Logged into Steam STEAMID: ' + client.steamID);
            console.log('[STEAM] ' + Config.botName + ' is ONLINE');
            client.setPersona(SteamUser.EPersonaState.Online, Config.botName);
            console.log('[STEAM] SteamName set');
            client.gamesPlayed("Hosting! [" + Config.modpackName + " " + Config.modpackVer + "]");
            console.log('[STEAM] GameName set');


            if (Config.announce)
            {
                if (Config.groupChatConsole)
                {
                    console.log('[STEAM] Notifiyng about server in group: ' + Config.chatGroupId);
                    client.chat.sendChatMessage(Config.announceChatGroupId, Config.announceChatRoomId, "Server UP! [" + Config.modpackName + " " + Config.modpackVer + "] IP: " + url.substr(6, url.length));
                } else
                {
                    Config.userAdmins.forEach(admin =>
                    {
                        if (admin.enabled
                        )
                        {
                            console.log('[STEAM] Notifiyng about server in private message to admin:' + admin.nickname);
                            client.chat.sendFriendMessage(admin.steamID3, "Server UP! [" + Config.modpackName + " " + Config.modpackVer + "] IP: " + url.substr(6, url.length));
                        }
                    })
                    ;
                }
            }
        });
    }
})();

/**
 * SERVER REMOTE
 */
server.stderr.on('data', (data) =>
{
    console.log(data.toString());
});
server.stdout.on('data', function (data)
{
    console.log(data);
});
Stdin.addListener('data', function (data)
{
    server.stdin.write(data.toString());
});

/**
 * STEAM CHAT REMOTE
 */
if (Config.useSteamChat)
{
    if (Config.groupChatConsole === false)
    {
        server.stdout.on('data', function (data)
        {
            Config.userAdmins.forEach(admin =>
            {
                if (admin.enabled)
                {
                    client.chat.sendFriendMessage(admin.steamID3, "[SERVER]: " + data);
                }
            });

            client.chat.addListener("friendMessage", function (d)
            {
                if (Config.debug)
                {
                    console.log(d);
                }
                console.log(Config.userAdmins.length);
                Config.userAdmins.forEach(admin =>
                {
                    if (admin.steamID3sim === d.steamid_friend.accountid.toString() && admin.enabled)
                    {
                        console.log("[" + admin.nickname + "]: " + d.message);
                        server.stdin.write(d.message.toString().replace("[Server thread/INFO]:", "") + "\n");
                    }
                });
            });
        })
    } else
    {
        server.stdout.on('data', function (data)
        {
            client.chat.sendChatMessage(Config.chatGroupId, Config.chatRoomId, "[SERVER]: " + data);
        });

        client.chat.addListener("chatMessage", function (d)
        {
            if (d.chat_group_id.toString() === Config.chatGroupId && d.chat_id.toString() === Config.chatRoomId)
            {
                server.stdin.write(d.message.toString() + "\n");
            }
            if (Config.debug)
            {
                console.log(d);
            }
        });
    }
}