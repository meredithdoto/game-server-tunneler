# Game server tunneler

Simple game server tunneler, for those who don't have public ip
Also with steam bot, notifies about that server is running and where + real time console

Install node [here](https://nodejs.org/en/)

use to download dependencies for steam and ngrok (using any terminal powerShell or Terminal itself)
```
npm update
```

You also need your server in ``server`` folder you put everything needed for server you want
eg.: 
- Minecraft you download [here](https://minecraft.net/en-us/download/server/)
- Configure tunneler
    - interpret ``java``
    - port ``25565``
    - args ``["-Xmx2048M", "-Xms2048M", "-jar", "server.jar", "nogui"]`` this is json array each word should be in quotes "" you can also see command on [here](https://minecraft.net/en-us/download/server/) to validate and understand how syntaxe works

After that just run in any console you like 
```
node app.js
```
or use any of ``start.bat`` or ``start.sh`` depending on your OS

You should also be friends or in group with bot otherwise you cannot accept messages

### Configuration

+ server folder - drop in your server scripts by default are executed from there
+ config.json - 
    + modpackName - server name in announce message
    + modpackVer - server version in annouce message
    + debug - can be used to read chat group/user ID
    + useSteamChat - Use steam chat bot?
    + groupChatConsole - Use user list or chat group as I/O
    + user - steam bot username
    + password - steam bot password
    + secret - if using mobile auth there goes secret - at the moment this is best way to authorize bot or you can disable steamGuard completely due to being only bot, otherwise I recommend to use [desktopAuthentificator](https://github.com/Jessecar96/SteamDesktopAuthenticator)
    + botName - name that bot should have
    + chatGroupId - ID of group chat set debug = true to get one
    + chatRoomId - ID of room in group chat set debug = true to get one
    + announce - announce server adress in chat group
    + announceChatGroupId - announce chat room ID
    + announceChatRoomId - announce chat group ID
    + userAdmins - Array of users 
        + enabled - use this user?
        + nickname - nickname to be displayed when command is sent to bot
        + steamID3sim - simplified steamID3 without ``[U:1:]`` eg.: ``[U:1:80821879]`` => ``80821879``
        + steamID3 - [Steam id finder](https://steamidfinder.com/)
        + steamID64 - [Steam id finder](https://steamidfinder.com/)
        + steamID - [Steam id finder](https://steamidfinder.com/)
    + server - 
        + region - Your region for tunnel (eu,us,ap,au)
        + authtoken - You also need to setup account on [ngrok](https://ngrok.com/) In part 3 (after you log in) copy the authcode           
        + interpret - launcher, if java use ``java`` if shell use ``sh`` etc.
        + port - port that game uses
        + args - arguments required to launch server